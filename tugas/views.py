from django.shortcuts import render,redirect

# Create your views here.
def home(request):
    return render(request,'pages/index.html')

def gallery(request):
    return render(request,'pages/gallery.html')

def story1(request):
    return render(request, 'pages/story1.html')