from django.urls import path
from . import views

app_name = 'tugas'

urlpatterns = [
    path('',views.home, name='index'),
    path('gallery', views.gallery, name='gallery'),
    path('story1', views.story1, name='story1'),
]